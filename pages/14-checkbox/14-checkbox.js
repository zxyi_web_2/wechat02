// pages/14-checkbox/14-checkbox.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    citys: [
      {id: 0, city: '武汉'},
      {id: 1, city: '广州'},
      {id: 2, city: '上海'},
      {id: 3, city: '深圳'},
    ],
    cityList:[]
  },
  selectItems(e) {
    console.log(e);
    const values = e.detail.value
    this.setData({
      cityList: values
    })
  }
})