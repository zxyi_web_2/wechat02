// pages/10-rich-text/10-rich-text.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // html: '<section data-v-448351b1="" class="category-list"><div data-v-448351b1=""><img data-v-448351b1="" src="//m.360buyimg.com/mobilecms/jfs/t25534/207/1767774998/8085/523157d6/5bbc800fN502129b8.png"><span data-v-448351b1="">Go超市</span></div><div data-v-448351b1=""><img data-v-448351b1="" src="//m.360buyimg.com/mobilecms/jfs/t21658/347/221358098/7461/f86e6f74/5b03b170Nc9e0ec7c.png"><span data-v-448351b1="">全球Go</span></div><div data-v-448351b1=""><img data-v-448351b1="" src="//m.360buyimg.com/mobilecms/jfs/t1/8385/17/3537/17895/5bd6ca67E09d23550/32d965fe9a9087a2.png"><span data-v-448351b1="">Go时尚</span></div><div data-v-448351b1=""><img data-v-448351b1="" src="//m.360buyimg.com/mobilecms/jfs/t17725/156/1767366877/17404/f45d418b/5ad87bf0N66c5db7c.png"><span data-v-448351b1="">Go生鲜</span></div><div data-v-448351b1=""><img data-v-448351b1="" src="//m.360buyimg.com/mobilecms/jfs/t16990/157/2001547525/17770/a7b93378/5ae01befN2494769f.png"><span data-v-448351b1="">Go到家</span></div><div data-v-448351b1=""><img data-v-448351b1="" src="//m.360buyimg.com/mobilecms/jfs/t18454/342/2607665324/6406/273daced/5b03b74eN3541598d.png"><span data-v-448351b1="">充值缴费</span></div><div data-v-448351b1=""><img data-v-448351b1="" src="//m.360buyimg.com/mobilecms/jfs/t22228/270/207441984/11564/88140ab7/5b03fae3N67f78fe3.png"><span data-v-448351b1="">9.9元拼</span></div><div data-v-448351b1=""><img data-v-448351b1="" src="//m.360buyimg.com/mobilecms/jfs/t25258/200/2527038521/14019/3d7a8470/5be92494N557a0c5b.png"><span data-v-448351b1="">领劵</span></div><div data-v-448351b1=""><img data-v-448351b1="" src="//m.360buyimg.com/mobilecms/jfs/t22120/183/200496447/5553/91ed22e0/5b03b7b8Ncea08c5b.png"><span data-v-448351b1="">省钱</span></div><div data-v-448351b1=""><img data-v-448351b1="" src="//m.360buyimg.com/mobilecms/jfs/t21481/263/412160889/15938/4246b4f8/5b0cea29N8fb2865f.png"><span data-v-448351b1="">全部</span></div></section>'
    html: [{
      name: 'div',
      attrs: {
        class: 'div_class',
        style: 'line-height: 60px; color: red;'
      },
      children: [{
        type: 'text',
        text: 'Hello&nbsp;World!'
      }]
    }]
  }
})