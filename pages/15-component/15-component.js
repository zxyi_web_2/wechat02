// pages/15-component/15-component.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    items: [
          {id: 0, name: '首页', isActive: true},
          {id: 0, name: '通讯录', isActive: false},
          {id: 0, name: '发现', isActive: false},
          {id: 0, name: '我的', isActive: false},
        ]
    },

    feathSelectItem(e) {
      // console.log('触发了父组件的方法');
      // console.log(e);
      
      /*
        得到 子组件传递 给父组件的 数据 index
      */ 
      const index = e.detail.index
      console.log(index);
      // 父组件自己的数据 自己控制数据状态
      // 排他算法 改变数据 数值 最好先 深拷贝数据 不改变原始数据， 直到数据并更完成 更新视图 时再去改变原始数据
      const list = JSON.parse(JSON.stringify(this.data.items))
      list.forEach((v, i) => i === index ? v.isActive =true : v.isActive= false)

      // 设置data并执行视图层渲染
      this.setData({
        items: list
      })

    }
})