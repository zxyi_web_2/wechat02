// pages/03-event/03-event.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    num: 0
  },
  /*
  input 输入时 触发的 事件方法
  e 是 输入的事件对象， input 输入的 值 在事件对象的 detail 属性中
  detail 中 value 就是 输入框中的值 

  */ 
  handelInput(e) {
    console.log(e.detail.value)
    // 要达到 输入框 输入的内容展示到 view 标签中连动的效果 就需要把 输入的值设置个 num

    // 将数据 赋值给 data 中的属性
    // 这种赋值不会触发界面 重新绘制 this.data.num = e.detail.value

    // this.setData 设置data 中的属性 可以触发界面重新绘制事件，界面重新绘制数据就更新到界面上
    // Number(e.detail.value) 将数据强制转换成数值类型
    this.setData({
      num: Number(e.detail.value)
    })
    console.log(this.data.num)
  },

  /*
  通过 标签自定义属性 data-[name] 传递 事件方法的参数

  */ 
  handelBtn(e) {
    console.log(e.currentTarget.dataset.opt)

    const count = e.currentTarget.dataset.opt
    this.setData({
      num: this.data.num + count
    })
  }
})