// pages/13-radio/13-radio.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sex: '男'
  },
  valueChange(e) {
    // console.log(e);
    const str = e.detail.value
    // console.log(str);
    this.setData({
      sex: str
    })
  }
})