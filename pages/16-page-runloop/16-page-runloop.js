// pages/16-page-runloop/16-page-runloop.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageList: ["/pages/16-page-runloop/16-page-runloop",
    "/pages/15-component/15-component",
    "/pages/14-checkbox/14-checkbox",
    "/pages/13-radio/13-radio",
    "/pages/12-icon/12-icon",
    "/pages/11-button/11-button",
    "/pages/10-rich-text/10-rich-text",
    "/pages/09-navigator/09-navigator",
    "/pages/08-swiper/08-swiper",
    "/pages/07-image/07-image",
    "/pages/06-text/06-text",
    "/pages/05-rpx-less/05-rpx-less", 
    "/pages/04-rpx/04-rpx",
    "/pages/03-event/03-event",
    "/pages/02-wx_if/02-wx_if",
    "/pages/01-wx_for/01-wx_for",]
  },

  /**
   * 生命周期函数--监听页面加载
   * 类似 vue 中的 created 钩子函数
   * 在页面 创建 时 只执行一次 
   */
  onLoad: function (options) {
    console.log('page: 1. 页面加载');
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   * 在页面 创建 时 只执行一次 
   */
  onReady: function () {
    console.log('page: 3.页面初次渲染完成');
  },

  /**
   * 生命周期函数--监听页面显示
   * 每一次页面展示 都会执行 onShow 方法
   */
  onShow: function () {
    console.log('page: 2. 页面显示');
    
  },
  /**
   * 生命周期函数--监听页面隐藏
   * 每一次页面隐藏都会触发这个方法
   */
  onHide: function () {
    console.log('page: 页面隐藏');
  },
// onShow onHide 是当前页面 显示或者隐藏 都会触发 包含 被其他页面盖住也属于隐藏


  /**
   * 生命周期函数--监听页面卸载
   * 当前back 的页面 会触发卸载方法
   */
  onUnload: function () {
    console.log('page: 页面卸载');
  },

  /*
  压栈： 先进后出
  队列： 先进先出
  */ 

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   * 允许用户 下拉操作 才能出发 这个方法 
   * 在页面配置文件中 配置 enablePullDownRefresh: true
   * 或者全局配置 app.json 中的 window 属性中 配置 enablePullDownRefresh: true
   */
  onPullDownRefresh: function () {
    console.log('page: 用户下拉动作');
    
  },

  /**
   * 页面上拉触底事件的处理函数
   *  上拉操作 配置标签距离(页面可以滚动才能出发) 页面底部 多远 来触发 事件
   * 在页面配置文件中 配置 onReachBottomDistance: 数值 单位px 单位省略
   * 或者全局配置 app.json 中的 window 属性中 配置 onReachBottomDistance: 数值 单位px 单位省略
   */
  onReachBottom: function () {
    console.log('page: 页面上拉触底事件');
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    console.log('page： 点击了分享按钮');
    
  },


//  onPageScroll	function			页面滚动触发事件的处理函数
onPageScroll() {
  // console.log('page: scroll');
  
},
// onResize	function			页面尺寸改变时触发，详见 响应显示区域变化
// 小程序中的页面支持屏幕旋转的方法是：在 app.json 的 window 段中设置 "pageOrientation": "auto" ，或在页面 json 文件中配置 "pageOrientation": "auto" 。
onResize() {
  console.log('page: 触发Resize');
  
},
// onTabItemTap	function			当前是 tab 页时，点击 tab 时触发
onTabItemTap() {
  console.log('page: TabItemTap tab中触发');
  
}
})