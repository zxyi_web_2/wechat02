// components/tabs/tabs.js
Component({
  /**
   * 组件的属性列表 接收父组件传递的值
   * list 是传递的属性  type是传递的数据类型， value是 数据的初始值
   */
  properties: {
    list: {
      type: Array,
      value: []
    }
  },

  /*
  properties: 父组件向子组件传递的属性值
  data： 组件内部自己的属性值
  访问方式一样


  console.log(this.properties);
  console.log(this.data);
  this.data 访问数据：
  this.data 先查找 自身设置的属性，如果没有 就去查找properties中 父组件传递的属性
  this.properties 只能访问 父组件传递过来的属性， 只能访问自己内部声明的属性
  */ 
  /**
   * 组件的初始数据
   */
  data: {
    // list: [
    //   {id: 0, name: '首页', isActive: true},
    //   {id: 0, name: '通讯录', isActive: false},
    //   {id: 0, name: '发现', isActive: false},
    //   {id: 0, name: '我的', isActive: false},
    // ]
  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 页面中的 方法 放置在 与 data 同一级，接在data属性后面
    // 注意： 组件中的 方法 放置在 methods 属性里面
    selectItem(e) {

        // console.log(this.properties);
        // console.log(this.data);
        
      // console.log(e);
      // 取出 数据索引 使用的是 结构
      const {index} = e.currentTarget.dataset
      // console.log(index);
      

      // 触发父组件的方法
      // 传递 触发的 数据索引给 父组件
      this.triggerEvent('selectItem', {index})

      // // 排他法
      // const {list} = this.data
      // /*
      //   对比遍历的数据索引， 如果 index === i 那么就将数据的 isActive 设置为 true
      //   否则就将 isActive 设置为 false
      // */ 
      // list.forEach((v, i) => index === i ? v.isActive=true : v.isActive=false)
      // // 将设置好的数据 设置回 data
      // this.setData({
      //   list
      // })
    },

  }
})
