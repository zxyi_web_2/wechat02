//app.js
App({
  // abc 制造编译阶段的错误

  /*
  onLaunch 生命周期回调——监听小程序初始化。
  小程序启动的时候 调用一次
  */ 	
  onLaunch() {
    console.log('app: 小程序初始化');
    // abc 运行阶段的错误
  },

  /*
  onShow 生命周期回调——监听小程序启动或切前台。
  切前台: 表示用户 肉眼可以看见的界面 叫做进入前台
  每一次 进入前台 都会触发 onShow方法
  */ 
  onShow() {
    console.log('app: 小程序启动或切前台');
  }	,
  /*
  onHide 	生命周期回调——监听小程序切后台。
  每一次 进入后台 都会触发 onHide 方法
  */ 	
  onHide() {
    console.log('app: 小程序切后台');
    
  },
  /*
  onError 	错误监听函数。
  当程序 *执行* 到 错误代码时候 会触发
  编译出错是不会 触发 onError方法 编译阶段的错 是还没有启动程序 编译工具就检测到了错误
  */ 	
  onError(err) {
    console.log('app: 这是 onError 的错误提示：', err);
    /*
      一般用来 记录 用户使用中 触发的 BUG ，把这些错误日志上传给后台记录， 前端开发通过记录的 错误日志分析用户触发的BUG 并修复
    */ 
  },
  // onPageNotFound 	页面不存在监听函数。	1.9.90
  onPageNotFound() {
    console.log('app: 不存在的页面');
    /*
      小程序 初始化 一个不存在的页面是触发 
      小程序 在 初次启动的时候  加载了一个 不存在的页面就会触发
    */ 
  },
  // onUnhandledRejection 	未处理的 Promise 拒绝事件监听函数。
  onUnhandledRejection() {
    console.log('app: 未处理的 Promise 拒绝事件监听函数');
    /*
     Promise 的 reject 回调没有处理 被触发就会 调用这个方法
    */ 
  }

})